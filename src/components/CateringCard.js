import React, {useState,useEffect} from "react";
import {Link} from "react-router-dom";

import '../App.css'

const CateringCard = (props) =>{
	console.log(props)
const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));

	return (
		<div className="card shadow my-3">
		  <div className="row no-gutters">
		    <div className="col-md-4">
		      <img src={props.cateringImage} className="card-img" alt="catering image" />
		    </div>
		    <div className="col-md-8">
		      <div className="card-body">
		        <h1 className="card-title">{props.packageName}</h1>
		        <p className="card-text">{props.description}</p>
		        <p className="card-text">Menu: {props.menu}</p>
		        <p className="card-text">Price: {props.pricePerHead} Pesos/head</p>

		      </div>
		      <div className="mx-auto text-center">

				{user && user.role==="admin" ?  
        			(
           				<Link to={"/update-catering-service/" + props.id}>
							<button type="submit" className="btn btn-primary text-light w-75 mx-auto my-1">Update Catering Service</button>	
						</Link>
          			):""}
			     	
					
				</div>
		    </div>
		    </div>
		</div>
		
	)
}

export default CateringCard;
