import React, { useState, useEffect} from "react";
import {Link, Redirect} from "react-router-dom";
import '../App.css'


const Nav = () =>{

  const [logoutSuccess, setLogoutSuccess] = useState(false);
  const [user, setUser] = useState(JSON.parse(localStorage.getItem("user")));
  // console.log(user)



  const logout = () => {
      localStorage.clear();
      setLogoutSuccess(true)
  }

  if(logoutSuccess){
    return <Redirect to="/" />
  }

	return (
		<nav className="navbar navbar-expand-lg navbar-light sticky-top">
 
 	 <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span className="navbar-toggler-icon"></span>
  </button>
  <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div className="navbar-nav w-100">
    
      <Link to="/">
         <a className="nav-item nav-link active">Home <span className="sr-only">(current)</span></a>
      </Link>

      <Link to="/catering">
        <a className="nav-item nav-link" href="#">Catering Services</a>
      </Link>

      
  
      {user && user.role==="user" ?  
        (
          <Link to="/contact-us">
            <a className="nav-item nav-link" href="#">Contact Us</a>
          </Link>
          ):""}


    <div className="ml-auto d-flex">

      {!user ?  
        (
          <Link  className="float-right" to="/login">
            <a className="nav-item nav-link mr-auto" href="#">Login</a>
          </Link>
          ):""}

      {!user ?  
        (
          <Link className="ml-auto" to="/register">
            <a className="nav-item nav-link" href="#">Register</a>
          </Link>
          )
         :""}
      </div >


      {user && user.role==="admin" ?  
        (
           <Link to="/create-venue">
              <a className="nav-item nav-link" href="#">Create Venue</a>
           </Link>
          )
         :""}
       

      {user && user.role==="admin" ?  
        (
          <Link to="/create-catering-service">
              <a className="nav-item nav-link" href="#">Create Catering Service</a>
          </Link>
          )
         :""}


      

      {user && user.role==="admin" ?  
        (
          <Link to="/bookings">
            <a className="nav-item nav-link" href="#">View All Bookings</a>
          </Link>
          )
         :""}
      

         
          

   


       

      {user ?  <button onClick={logout} className="nav-item nav-link">Logout</button> :""}
       
      
      

    </div>
  </div>
 
</nav>

		)
}

export default Nav;
