import React from "react";
import '../App.css';

const Banner = () =>{
	return (
		<div className="banner text-center">
			<img className="logo" src="/images/logo.jpeg" alt=""/>
			<h3 className="company-name">Kairos Special Event Venue</h3>
		</div>
		)
}

export default Banner;