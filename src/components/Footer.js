import React from "react";

const Footer = () => {
return(

<div className="container-fluids">
 <div className="footer-bar">
   <div className="container">
     <div className="row py-4 d-flex align-items-center">
       <div className="col-md-6 text-center text-md-left mb-4 mb-md-0">
         <h6 className="mb-0">Get connected with us on social networks!</h6>
       </div>
       <div className="col-md-6 text-center text-md-right">
         <a className="fb-ic">
           <i className="fab fa-facebook-f white-text mr-4"> </i>
         </a>
       
         <a className="tw-ic">
           <i className="fab fa-twitter white-text mr-4"> </i>
         </a>
     
         <a className="gplus-ic">
           <i className="fab fa-google-plus-g white-text mr-4"> </i>
         </a>
         
         <a className="li-ic">
           <i className="fab fa-linkedin-in white-text mr-4"> </i>
         </a>
       
         <a className="ins-ic">
           <i className="fab fa-instagram white-text"> </i>
         </a>

       </div>
     

     </div>
 

   </div>
 </div>


 <div className="container text-center text-md-left mt-5">

 
   <div className="row mt-3">

 
     <div className="col-md-3 mx-auto mb-4">

     
       <h6 className="text-uppercase font-weight-bold">Kairo's special Event Place</h6>
       <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
       <small>Celebrate your milestones a whole new way!
          Make your special occasions more meaningful and memorable here at Kairo's Special Events Place, a sprawling garden venue in the heart of a bustling metropolis.  It boasts of artistically landscaped surroundings and water features–ponds, fountains and waterfalls–truly a breath of fresh air amidst the concrete jungle. </small>

     </div>
   
     <div className="col-md-2 mx-auto mb-4">

       <h6 className="text-uppercase font-weight-bold">Photographers</h6>
       <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
       <p>
         <a href="#!">Ced Tabares Photography</a>
       </p>
       <p>
         <a href="#!">Orapa Digital Photography</a>
       </p>
       <p>
         <a href="#!">Ethan Cruz Photo Gallery</a>
       </p>

     </div>

     <div className="col-md-3 mx-auto mb-4">

     
       <h6 className="text-uppercase font-weight-bold">Catering Affiliates</h6>
       <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto"/>
       <p>
         <a href="#!">Hizon's Catering</a>
       </p>
       <p>
         <a href="#!">Eloquente Catering</a>
       </p>
       <p>
         <a href="#!">CVJ Food Catering</a>
       </p>
       <p>
         <a href="#!">Help</a>
       </p>

     </div>
   

   
     <div className="col-md-4 mx-auto mb-md-0 mb-4">

       <h6 className="text-uppercase font-weight-bold">Contact</h6>
       <hr className="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" />
       <p>
         <i className="fas fa-home mr-3"></i> Mindanao Ave, QC</p>
       <p>
         <i className="fas fa-envelope mr-3"></i> kairoevents@gmail.com</p>
       <p>
         <i className="fas fa-phone mr-3"></i> + 781-8012554</p>
       

     </div>
 

   </div>
 

 </div>



 <div className="text-center py-3">© 2019 Copyright:
   <a href="#"> Kairo's Special Event</a>
 </div>


</div>

)
}

export default Footer;

