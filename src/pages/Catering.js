import React from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import '../App.css'

// components
import Banner from "../components/Banner";  
import Nav from "../components/Nav";
import Hero from "../components/Hero";
import CateringCard from "../components/CateringCard"
import Footer from "../components/Footer";

// query
import {getCateringServicesQuery} from "../queries/queries";

const Catering = (props) =>{

	const cateringServicesData = props.getCateringServicesQuery.getCateringServices ? props.getCateringServicesQuery.getCateringServices:[]
	
	return (
		<div>
			<Banner />

		    <Nav />

		    <Hero />

			<div className="container">	
				<h1 className="text-center my-3">Catering Services</h1>	
				<div className="row">
				{ cateringServicesData.map((cateringService)=>{
					return(
						<div className="col-12">
							<CateringCard 
							cateringImage="/images/kidsparty.jpeg" 
							packageName={cateringService.packageName} 
							description={cateringService.description}
							menu={cateringService.menu}

							pricePerHead={cateringService.pricePerHead}
							id={cateringService.id}
							/>
						</div>
						)
					})
				}
				</div>
			</div>
	
		</div>
		
		)
}

export default compose(graphql(getCateringServicesQuery, { name: "getCateringServicesQuery"}))(Catering);

