import React, { useState, useEffect} from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom';

import Banner from "../components/Banner";
import Nav from "../components/Nav";

// mutation
import { createCateringServiceMutation } from "../queries/mutations";

// query
import {getCateringServicesQuery} from "../queries/queries";

const CateringServiceCreatePage = (props) =>{

	// hooks
	const [packageName, setPackageName] = useState("");
	const [pricePerHead, setPricePerHead] = useState("");
	const [description, setDescription] = useState("");
	const [menu, setMenu] = useState("");
	const [gotoLogin, setGotoLogin] = useState(false);
    const [isDisabled, setIsDisabled] = useState(true);

    useEffect(()=>{
    	console.log(packageName)
    	console.log(pricePerHead)
    	console.log(description)
    	console.log(menu)
    	
    })

	const packageNameChangeHandler = e => {
		setPackageName(e.target.value);
	};

	const pricePerHeadChangeHandler = e => {
		setPricePerHead(e.target.value);
	};

	const descriptionChangeHandler = e => {
		setDescription(e.target.value);
	};

	const menuChangeHandler = e => {
		setMenu(e.target.value);
	};

	const addCateringService = e => {
		e.preventDefault();
		

		props.createCateringServiceMutation({
			variables: {
				packageName: packageName,
				pricePerHead: parseInt(pricePerHead),
				description: description,
				menu: menu
				
			},
			refetchQueries:[{
				query: getCateringServicesQuery
			}]
			
		}).then((response) => {
			console.log(response.data)
        	const cateringServiceAdded = response.data.createCateringService
        
        if (cateringServiceAdded) {
            Swal.fire({
                title: 'Created Catering Service Successfully',
                // text: 'You will now be redirected to the login.',
                icon: 'success'
            }).then(() => {
                setGotoLogin(true)
                // console.log("should go to login page")
            }).catch(err=>{
            	console.log(err)
            })
        } else {
            Swal.fire({
                title: 'Failed',
                icon: 'error'
            })
        }
    })

	}	

	if(gotoLogin){
		return <Redirect to="/catering" />
	}

	return (
		<section >
			<Banner />

			<Nav />

			
			<div>

			 	<div className="container my-3 py-5">
					<h1 className="text-center my-2">Create Catering Service</h1>

					<form onSubmit={addCateringService} className=" mx-auto">
						<div className="row">
							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="packageName">Catering Package Name:</label>
									<input 
										className="form-control" 
										type="text" 
										id="packageName"
										onChange={packageNameChangeHandler}
										value={packageName}
										/>
								</div>
							</div>	

							<div className="col-12 col-md-6">
								<div className="form-group">
									<label htmlFor="pricePerHead">Price per Head: </label>
									<input 
										className="form-control" 
										type="text" 
										id="pricePerHead"
										onChange={pricePerHeadChangeHandler}
										value={pricePerHead}
										/>
								</div>
							</div>	

									
						</div>


						<div className="row">
							<div className="col-12">
								<label htmlFor="cateringServiceDescription">Description:</label>
								<textarea 
									className="form-control" 
									id="cateringServiceDescription" 
									cols="100" rows="5"
									onChange={descriptionChangeHandler}
									value={description}
									>
								</textarea>
							</div>
						</div>

						<div className="row">
							<div className="col-12">
								<label htmlFor="menu">Menu:</label>
								<input 
									className="form-control" 
									type="text" 
									id="menu" 
									placeholder="add here"
									onChange={menuChangeHandler}
									value={menu}
									/>
		
							</div>
						</div>

						<button type="submit" className="btn btn-primary text-light w-100 mx-auto my-1">Add Catering Service</button>	
							
					</form>
				</div>

			</div>

			
		</section>
		
		)
}

export default compose(graphql(createCateringServiceMutation, { name: "createCateringServiceMutation"}),
	graphql(getCateringServicesQuery, {name: "getCateringServicesQuery"})
	)(CateringServiceCreatePage);
