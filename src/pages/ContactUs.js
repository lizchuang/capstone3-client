import React from "react";

import '../App.css'

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Hero from "../components/Hero";

import Footer from "../components/Footer";

const ContactUs = () =>{
	return (
		<div>
			<Banner />

		    <Nav />

		    <Hero />

					
			<div className="container shadow-lg">
				<h1 className=" contactUs text-center my-5">Contact Us</h1>

				<form className="w-75 mx-auto">
					<div className="row">
						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="name">Name</label>
								<input className="form-control" type="text" id="name"/>
							</div>
						</div>	

						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="event-type">Event Type</label>
								<input className="form-control" type="text" id="event-type"/>
							</div>
						</div>					
					</div>

					<div className="row">
						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="email">Email</label>
								<input className="form-control" type="text" id="email"/>
							</div>
						</div>	

						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="guests">Number of Guests</label>
								<input className="form-control" type="text" id="guests"/>
							</div>
						</div>					
					</div>

					<div className="row">
						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="contact">Contact Number</label>
								<input className="form-control" type="text" id="contact"/>
							</div>
						</div>	

						<div className="col-12 col-md-6">
							<div className="form-group">
								<label htmlFor="date">Date of Event</label>
								<input className="form-control" type="text" id="date"/>
							</div>
						</div>					
					</div>

					<div className="row">
						<div className="col-12">
							<div className="form-group">
								<label htmlFor="message">Message</label>
								<textarea className="form-control" id="message" cols="100" rows="8">
								</textarea>
							</div>
							
						</div>
						
					</div>


				</form>

						

			</div>

			
		</div>
		
		)
}

export default ContactUs;
