import React, { useState, useEffect} from "react";
import '../App.css';
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";
import { Redirect } from 'react-router-dom';

import Banner from "../components/Banner";
import Nav from "../components/Nav"; 

// getTransactionsQuery
import {getTransactionsQuery} from "../queries/queries";

const Bookings = (props) =>{

	// hooks
	// const[bookings, setBookings]= useState([])
	// // console.log(props)
	const bookingsData=props.getTransactionsQuery.getTransactions ? props.getTransactionsQuery.getTransactions:[]

	// console.log(bookingsData)
	let bookings =()=>{
	if(props.getTransactionsQuery.loading){
		console.log("fetching transactions")
		return <p>fetching transactions</p>
	}else{
		// setBookings(bookingsData)
		return bookingsData;
	}
	
	}
	
	return (
		<section >
			<Banner />

			<Nav />
			<div className="container">
				<h1 className="text-center my-4">List of Bookings</h1>
				<div className="row">
					<div className="col-12">

	
			<table class="table table-hover">
  				<thead>
    				<tr>
      					<th scope="col">Booking #</th>
     					<th scope="col">Event Type</th>
     					<th scope="col">Date</th>
     					<th scope="col">Time</th>
     					<th scope="col">Venue</th>
     					<th scope="col">Catering Service</th>
     					
     					<th scope="col">Action</th>
    				</tr>
  				</thead>
  				
  				<tbody>
  				{ bookingsData.map((booking)=>{
  					console.log(booking)
						return(
						<tr>
							<th scope="row">1</th>
    						<td>{booking.eventType}</td>
							<td>{booking.date}</td>
     						<td>{booking.time}</td>
     						<td>{booking.venue}</td>
     						<td>{booking.cateringService}</td>
     						<td>
     							<button className="btn btn-primary">Approved</button>
     							<button className="btn btn-danger">Reject</button>
     						</td>
    					</tr>

						);
    				})
    			}
   					
  				</tbody>
			</table>
			</div>
			</div>
			</div>
			
		</section>
		
		)
}

export default compose(graphql(getTransactionsQuery,{name:"getTransactionsQuery"}))(Bookings);


