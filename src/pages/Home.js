import React from "react";
import { graphql } from "react-apollo";
import Swal from "sweetalert2";
import { flowRight as compose } from "lodash";

import '../App.css'

// components
import Banner from "../components/Banner";
import Nav from "../components/Nav";
import Hero from "../components/Hero";
import VenueCard from "../components/VenueCard";
import Footer from "../components/Footer";

// query
import {getVenuesQuery} from "../queries/queries";

const Home = (props) =>{

	const venueData = props.getVenuesQuery.getVenues ? props.getVenuesQuery.getVenues:[]

	return (
		<div>
			      <Banner />
			      <Nav />
			      <Hero />
					<div className="container my-3 ">
						<div className="row">
						{
							venueData.map((venue)=>{
							return(
							<div className="col-12 col-md-4">
								<VenueCard 
									venueImage="/images/functionhall1.jpeg" 
									name={venue.name} 
									description={venue.description} 
									link={"/venue/" + venue.id}
								/>
							</div>
							)
							})
						 }
							
			    	 	</div>
			    	 </div>			    	 
		</div>
		
		
		)
}

export default compose (graphql(getVenuesQuery, {name:"getVenuesQuery"}))(Home);