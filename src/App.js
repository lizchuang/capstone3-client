import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";



// pages
import Home from "./pages/Home";
import Venue from "./pages/Venue";
import ContactUs from "./pages/ContactUs";
import Catering from "./pages/Catering";
import Register from "./pages/Register";
import Footer from "./components/Footer";
import Login from "./pages/Login";
import VenueCreatePage from "./pages/VenueCreatePage";
import CateringServiceCreatePage from "./pages/CateringServiceCreatePage";
import UpdateVenue from "./pages/UpdateVenue";
import BookVenue from "./pages/BookVenue";
import Bookings from "./pages/Bookings";
import UserBooking from "./pages/UserBooking";
import UpdateCateringService from "./pages/UpdateCateringService";

const client = new ApolloClient({ uri: "https://kairo-events.herokuapp.com/graphql" });

function App() {
  return (
    <ApolloProvider client={client}>
    
    	<BrowserRouter>
    		<Route exact path="/" component={Home} />
    		<Route path="/venue/:id" component={Venue} />
    		<Route path="/contact-us" component={ContactUs} />
    		<Route path="/catering" component={Catering} />
    		<Route path="/register" component={Register} />
    		<Route path="/login" component={Login} />
            <Route path="/create-venue" component={VenueCreatePage} />
            <Route path="/create-catering-service" component={CateringServiceCreatePage} />
            <Route path="/update-venue/:id" component={UpdateVenue} />
            <Route path="/book-venue/:id" component={BookVenue} />
            <Route path="/bookings" component={Bookings} />
            <Route path="/user-booking/:id" component={UserBooking} />
            <Route path="/update-catering-service/:id" component={UpdateCateringService} />
        <Footer />
    	</BrowserRouter>
     </ApolloProvider>      
   
  );
}

export default App;
