import { gql } from "apollo-boost";

const getVenuesQuery = gql`
query
{
  getVenues{
    id
    name
    floorArea
    maxGuest
    description
    price
  }
}
`;


const getCateringServicesQuery= gql`
query
{
  getCateringServices{
    id
    packageName
    pricePerHead 
    description
    menu
  }
}
`;

const getVenueQuery= gql`
query
(
  $id: String!
)
{
  getVenue
  (
    id: $id
    
  ){
    id
    name
    floorArea
    maxGuest
    description
    price
  }  
}

`;

const getTransactionsQuery= gql`
query
{
  getTransactions{
    id
    venueId
    venue
    userId
    eventType
    guestNumber
    cateringService
    date
    time
  }
}

`;

const getTransactionQuery= gql`
query
{
  getTransaction
  (
  id: $id
    
  )
}

`;


const getCateringServiceQuery=gql`
query(
  $id: ID!
){
  getCateringService
  (
    id: $id
     
  ){
    id
    packageName
    pricePerHead
    description
    menu
    
  }
}


`;



export {
  getVenuesQuery, 
  getCateringServicesQuery,
  getVenueQuery,
  getTransactionsQuery,
  getTransactionQuery,
  getCateringServiceQuery
};