import { gql } from "apollo-boost";

const createUserMutation = gql`
mutation(
		$firstName: String!
		$lastName: String!
		$contactNumber: String
		$email: String!
		$password: String!
		$role: String

	){
	  createUser(
	    firstName: $firstName
	  	lastName: $lastName
	  	contactNumber: $contactNumber
	    email: $email
	    password: $password
	    role: $role
	  ){
	    id
	    firstName
	    lastName
	    contactNumber
	    email
	    password
	    role
	  }
	}
`;

const createVenueMutation= gql`
mutation(
	$name: String!
	$floorArea: Int!
	$maxGuest: Int!
	$description: String!
	$price: Int!

){
  createVenue(
    name: $name
    floorArea: $floorArea
    maxGuest: $maxGuest
    description: $description
    price: $price
  ){
    id
    name
    floorArea
    maxGuest
    description
    price
  }
  
}

`;

const createCateringServiceMutation= gql`
mutation(
	$packageName: String!
	$pricePerHead: Int!
	$description: String!
	$menu: String!
){
  createCateringService(
    packageName: $packageName
    pricePerHead: $pricePerHead
    description:$description
    menu: $menu
  ){
    id
    packageName
    pricePerHead
    description
    menu
  }
}

`;

const updateVenueMutation= gql`
mutation(
    $id: String!
    $name: String!
    $floorArea: Int!
    $maxGuest: Int!
    $description: String!
  	$price: Int!
 )

{
  updateVenue(
    id: $id
    name: $name
    floorArea: $floorArea
    maxGuest: $maxGuest
    description: $description
    price: $price
    
    
  ){
    id
    name 
    floorArea
    maxGuest
    description
  	price
  }
}
`;

const createTransactionMutation = gql `
mutation(
    $venueId: String!
    $userId: String
    $eventType: String!
    $guestNumber: Int!
    $cateringService: String!
    $date: Date!
    $time: String!

){
  createTransaction(
    venueId: $venueId
    userId: $userId
    eventType: $eventType
    guestNumber: $guestNumber
    cateringService: $cateringService
    date: $date
    time: $time
    
  ){
    id
    venueId
    userId
    eventType
    guestNumber
    cateringService
    date
    time
  }
}

`;

const loginUserMutation = gql`
mutation(
  $email: String!
  $password: String!

)

{
  logInUser
  (
    email: $email
    password: $password
  ){
    id
    firstName
    lastName
    contactNumber
    email
    role
  }
}

`;

const updateCateringServiceMutation= gql`
mutation(
    $id: String!
    $packageName: String!
    $pricePerHead: Int!
    $description: String!
    $menu: String!
    ){
   updateCateringService(
    id: $id
    packageName: $packageName
    pricePerHead: $pricePerHead
    description: $description
    menu: $menu   
  ){
    id
    packageName 
    pricePerHead
    description
    menu
  }
}


`;

const deleteVenueMutation = gql`
mutation(
  $id: String!
  ){
  deleteVenue(
    id: $id
  )
}

`;

const deleteCateringServiceMutation = gql`
mutation(
  $id: String!
){
  deleteCateringService(
    id: $id
  )
}

`;



export { createUserMutation,
 createVenueMutation, 
 createCateringServiceMutation, 
 updateVenueMutation, 
 createTransactionMutation,
 loginUserMutation,
 updateCateringServiceMutation,
 deleteVenueMutation,
 deleteCateringServiceMutation
  }